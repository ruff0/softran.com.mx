<?php

/**
 * sharai_khana functions and definitions
 *
 * @package sharai-khana
 */
/**
 * Set the content width based on the theme's design and stylesheet.
 */

// DEFINE THEME CONSTANTS
define('SHARAI_KHANA_THEME_PRIMARY_COLOR', '#80B435'); 
define('SHARAI_KHANA_THEME_TEXT_COLOR', '#555555'); 
define('SHARAI_KHANA_THEME_LINK_HOVER_COLOR', '#80B435'); 

// Register all the theme options
require_once( get_template_directory() . '/inc/redux-config.php' );

// Theme options functions
require_once( get_template_directory() . '/inc/sharai-khanawp-options.php' );

if (!isset($content_width)) {
    $content_width = 640; /* pixels */
}

if (!function_exists('sharai_khana_add_editor_styles')):

    function sharai_khana_add_editor_styles() {
        add_editor_style('custom-editor-style.css');
    }

endif;
add_action('admin_init', 'sharai_khana_add_editor_styles');

if (!function_exists('sharai_khana_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function sharai_khana_setup() {
    
        define('SHARAI_KHANA_THEME_VER', '20180114-1.0.0');
        define('SHARAI_KHANA_CMB_PREFIX', 'sharai_khana_'); // custom meta box prefix.

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on sharai_khana, use a find and replace
         * to change 'sharai-khana' to the name of your theme in all the template files
         */
        load_theme_textdomain('sharai-khana', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        //For Blog Single Post Thumb.

        add_image_size('sharai_khana-blog-single-large', 850, 430); // Soft proprtional crop to max 720px width, max 340px height

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary Menu', 'sharai-khana'),
        ));

        register_nav_menus(array(
            'footer-menu' => esc_html__('Footer Menu', 'sharai-khana'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
        ));

        /*
         * Enable support for Post Formats.
         * See http://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside', 'image', 'video', 'quote', 'link',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('sharai_khana_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));
        
    }

endif; // sharai_khana_setup
add_action('after_setup_theme', 'sharai_khana_setup');

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function sharai_khana_widgets_init() {
    
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'sharai-khana'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Main Sidebar Area', 'sharai-khana'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    
}

add_action('widgets_init', 'sharai_khana_widgets_init');


/**
 * Register Google Fonts
*/
function sharai_khana_fonts_url() {
    
    $font_url = '';
    
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'sharai-khana' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Fjalla+One|Lato:300,400,400i,700,700i' ), "//fonts.googleapis.com/css" );
    }
    
    return $font_url;
}

/**
 * Enqueue scripts and styles.
 */
function sharai_khana_scripts() {
    
    if (!class_exists('Redux')) {
    
        wp_enqueue_style( 'sharai-khana-fonts', sharai_khana_fonts_url(), array(), '1.0.0' );
    
    }
    
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.4', 'all');

    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.3.0', 'all');

    if (!class_exists('Redux')) {
        
        wp_enqueue_style('sharai-khana-defaults', get_template_directory_uri() . '/css/option-panel-default.css', array(), '1.0.0', 'all');
        
    }

    wp_enqueue_style('sharai-khana-style', get_stylesheet_uri());
    
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.4', TRUE);
    
    wp_enqueue_script('jquery-superfish', get_template_directory_uri() . '/js/superfish.min.js', array('jquery'), SHARAI_KHANA_THEME_VER, true);
    
    wp_enqueue_script('sharai-khana-custom-scripts', get_template_directory_uri() . '/js/custom-scripts.js', array(), SHARAI_KHANA_THEME_VER, true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'sharai_khana_scripts');

/**
 * WooCommerce Integration
 */
add_action('after_setup_theme', 'sharai_khana_woocommerce_support');

function sharai_khana_woocommerce_support() {
    add_theme_support('woocommerce');
}


// Total Number of products display per page.

add_filter( 'loop_shop_per_page', 'sharai_khana_loop_shop_per_page', 20 );

function sharai_khana_loop_shop_per_page( ) {
  $cols = 6;
  return $cols;
}

// Total Number of products display per row.

add_filter( 'loop_shop_columns', 'sharai_khana_loop_shop_columns', 20 );

function sharai_khana_loop_shop_columns( ) {
  return 3;
}

/** remove redux menu under the tools * */
add_action('admin_menu', 'sharai_khana_remove_redux_menu', 12);

function sharai_khana_remove_redux_menu() {
    remove_submenu_page('tools.php', 'redux-about');
}

/**
 * Implement the Custom Header feature.
 */
require_once get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require_once get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require_once get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require_once get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require_once get_template_directory() . '/inc/jetpack.php';

/**
 * Load Bootstrap Menu.
 */
require_once get_template_directory() . '/inc/bootstrap-walker.php';

/**
 * Comments Callback.
 */
require_once get_template_directory() . '/inc/comments-callback.php';

/**
 * Search Results - Highlight.
 */
require_once get_template_directory() . '/inc/search-highlight.php';

/**
 * Template Home Page - dynamic widget.
 */
require_once get_template_directory() . '/inc/custom-widgets.php';

/**
 * Set custom metabox for sharai_khana
 */
require_once get_template_directory() . '/inc/custom-meta-box-sharai-khana.php';

//ADD CUSTOM CSS.
require_once get_template_directory() . '/inc/admin-custom-css.php';

//ADD EXTERNAL PLUGINS
require_once get_template_directory() . '/inc/required-plugin-tgm.php';

//ADD ONE CLICK DEMO IMPORTER
require_once get_template_directory() . '/inc/one-click-importer.php';
